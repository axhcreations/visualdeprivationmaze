﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
    public float wraithRange = 25;
    public float playerVolume = 20;
    public int numberOfWalls =0;
    public float wallBlockAmount = 10;
	// Use this for initialization
	void TheTest () {

        float d = Random.Range(0,100);
        float newRange = wraithRange-(numberOfWalls*wallBlockAmount);
        bool canHear =  (d - playerVolume) < newRange?true:false;
        Debug.LogError ( "Wraith hearing distance = " + wraithRange );
        Debug.LogError ( "Distance between Wraith and Player = " + d  );        
        Debug.LogError ( string.Format( "Number of walls = {0} , reducing wraith hearing range from {1} to {2}", numberOfWalls,wraithRange , newRange ) );
        Debug.LogError ( string.Format ( "(Distance={0} - PlayerVolume={1} = {2}) < WraithRange={3} " , d , playerVolume , (d - playerVolume) , newRange ) );        
        Debug.LogError ( "Can hear = " + canHear );
        if ( canHear == false )
            Debug.LogError ( (d-playerVolume)- newRange +" away from hearing player");
    }
	
	// Update is called once per frame
	void Update () {
        if ( Input.GetKeyDown ( KeyCode.Space ) ) {
            TheTest ();
        }
	}
}
