﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour {

    [SerializeField]
    private bool _generateEmptyMaze = false;
    public bool mazeGenerated { private set; get; }
    public bool createRandomSeed = false;
    public int mazeRows, mazeColumns;
    private GameObject mazeHolder, mazeWalls, mazeFloors;
    public GameObject wallPrefab;
    public float size = 2f;

    private MazeCell[,] mazeCells;

    public void GenerateMaze ( bool debugMode = false ) {

        if ( debugMode ) {
            createRandomSeed = false;
            _generateEmptyMaze = true;
            mazeRows = 6;
            mazeColumns = 6;
        }

        if ( createRandomSeed ) {
            ProceduralNumberGenerator.GenerateRandomKey ();
        }
        
        if ( !debugMode ) {           
            InitMaze ( RunMazeAlgorithm );     
        }
        else {
            InitMaze ( MazeCreationComplete );
        }
    }

    private void RunMazeAlgorithm ( ) {
        MazeAlgorithm ma = new HuntAndKillMazeAlgorithm (mazeCells);
        ma.CreateMaze ( MazeCreationComplete );
    }

    private void MazeCreationComplete ( ) {
        Debug.Log ( "Maze creation complete" );
        mazeGenerated = true;
    }

    public MazeCell GetFirstCell ( ) {
        return mazeCells [ 0 , 0 ];
    }

    public MazeCell GetLastCell ( ) {
        return mazeCells [ mazeCells.GetLength ( 0 ) - 1 , mazeCells.GetLength ( 1 ) - 1 ];
    }

    private void InitMaze ( System.Action onComplete = null ) {
        mazeCells = new MazeCell [ mazeRows, mazeColumns ];
        mazeHolder = new GameObject();
        mazeHolder.name = "Maze";
        mazeWalls = new GameObject();
        mazeWalls.name = "Walls";
        mazeWalls.transform.SetParent ( mazeHolder.transform );
        mazeFloors = new GameObject();
        mazeFloors.name = "Floor";
        mazeFloors.transform.SetParent ( mazeHolder.transform );

        for ( int r = 0 ; r < mazeRows ; r++ ) {
            for ( int c = 0 ; c < mazeColumns ; c++ ) {
                mazeCells [ r , c ] = new MazeCell ();

                // For now, use the same wall object for the floor!
                mazeCells [ r , c ].floor = Instantiate ( wallPrefab , new Vector3 ( r * size , 0 , c * size ) , Quaternion.identity , mazeFloors.transform ) as GameObject;
                mazeCells [ r , c ].floor.name = "Floor " + r + "," + c;
                mazeCells [ r , c ].floor.transform.Rotate ( Vector3.right , 90f );

                if ( c == 0 ) {
                    mazeCells [ r , c ].westWall = Instantiate ( wallPrefab , new Vector3 ( r * size , size / 2f , (c * size) - (size / 2f) ) , Quaternion.identity , mazeWalls.transform ) as GameObject;
                    mazeCells [ r , c ].westWall.name = "West Wall " + r + "," + c;
                }

                if ( !_generateEmptyMaze ) {
                    mazeCells [ r , c ].eastWall = Instantiate ( wallPrefab , new Vector3 ( r * size , size / 2f , (c * size) + (size / 2f) ) , Quaternion.identity , mazeWalls.transform ) as GameObject;
                    mazeCells [ r , c ].eastWall.name = "East Wall " + r + "," + c;
                }
                else {
                    if ( c == (mazeCells.GetLength ( 1 ) - 1) ) {
                        mazeCells [ r , c ].eastWall = Instantiate ( wallPrefab , new Vector3 ( r * size , size / 2f , (c * size) + (size / 2f) ) , Quaternion.identity , mazeWalls.transform ) as GameObject;
                        mazeCells [ r , c ].eastWall.name = "East Wall " + r + "," + c;
                    }
                }

                if ( r == 0 ) {
                    mazeCells [ r , c ].northWall = Instantiate ( wallPrefab , new Vector3 ( (r * size) - (size / 2f) , size / 2f , c * size ) , Quaternion.identity , mazeWalls.transform ) as GameObject;
                    mazeCells [ r , c ].northWall.name = "North Wall " + r + "," + c;
                    mazeCells [ r , c ].northWall.transform.Rotate ( Vector3.up * 90f );
                }
                if ( !_generateEmptyMaze ) {
                    mazeCells [ r , c ].southWall = Instantiate ( wallPrefab , new Vector3 ( (r * size) + (size / 2f) , size / 2f , c * size ) , Quaternion.identity , mazeWalls.transform ) as GameObject;
                    mazeCells [ r , c ].southWall.name = "South Wall " + r + "," + c;
                    mazeCells [ r , c ].southWall.transform.Rotate ( Vector3.up * 90f );
                }
                else {
                    if ( r == (mazeCells.GetLength ( 0 ) - 1) ) {
                        mazeCells [ r , c ].southWall = Instantiate ( wallPrefab , new Vector3 ( (r * size) + (size / 2f) , size / 2f , c * size ) , Quaternion.identity , mazeWalls.transform ) as GameObject;
                        mazeCells [ r , c ].southWall.name = "South Wall " + r + "," + c;
                        mazeCells [ r , c ].southWall.transform.Rotate ( Vector3.up * 90f );
                    }
                }
            }
        }
        if ( onComplete != null ) {
            onComplete.Invoke ();
        }
    }
}
