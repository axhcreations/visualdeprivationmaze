﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour {

    public bool playerControlEnabled = false;
    [HideInInspector]
    public static Transform playerPosition;


    public KeyCode footstepKeycode;
    public KeyCode audioEmitterKeycode;
    public GameObject lightCubePrefab;
    public float lightCubeThrowStrength = 5;
    public Transform playerHead;

    public float movingAudioRange;
    public float footstepAudioRange;
    public float emitterAudioRange;
    public float combinedRange;

    private PlayerMotor _motor;
    [SerializeField]
    public float audioDistance;
    [SerializeField]
    private bool _isStepping = false;
    [SerializeField]
    private bool _isEmitting = false;
    private WraithController _wraith;

    private AudioEmitterDistanceVolumeChanger _audioEmitter;

    public Material blockingSphereMat;

    private void Awake ( ) {
        blockingSphereMat.DOColor ( Color.white , 0f );
        _motor = GetComponent<PlayerMotor> ();
        _audioEmitter = FindObjectOfType<AudioEmitterDistanceVolumeChanger> ();
        playerPosition = this.transform;
    }

    void EnableBlockingSphere (bool enable ) {
        if ( enable )
            blockingSphereMat.DOColor ( Color.white ,5f);
        else
            blockingSphereMat.DOColor ( Color.clear , 5f );
    }

    public void EnablePlayer ( bool newState ) {
        _motor.SetEnabledState ( newState );
        playerControlEnabled = newState;
        EnableBlockingSphere ( !newState );
    }

    void SetAudioRange ( ) {
        if ( _isEmitting && _isStepping )
            audioDistance = combinedRange;
        else if ( _isEmitting )
            audioDistance = emitterAudioRange;
        else if ( _motor.isMoving ) {
            
            if ( _isStepping )
                audioDistance = footstepAudioRange;
            else
                audioDistance = movingAudioRange;
        }
        else {
            audioDistance = 1f;
        }
    }

    void Update ( ) {
        SetAudioRange ();
        if ( playerControlEnabled ) {
            bool stepping  = Input.GetKey ( footstepKeycode );
            if ( _isStepping != stepping ) {
                _isStepping = stepping;
                _motor.visualiseFootSteps = _isStepping;
            }
            bool emitting =  Input.GetKey ( audioEmitterKeycode );

            if ( _isEmitting != emitting ) {
                _isEmitting = emitting;
                _audioEmitter.SetEnabledState ( _isEmitting );
            }
        }
    }

    private void OnDrawGizmos ( ) {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere ( transform.position , audioDistance );
    }

    private void ThrowLightCube ( ) {
        GameObject lightCube = Instantiate(lightCubePrefab, transform.position + (Vector3.up),Quaternion.identity);
        lightCube.GetComponent<Rigidbody> ().AddForce ( playerHead.forward * lightCubeThrowStrength );
    }
}
