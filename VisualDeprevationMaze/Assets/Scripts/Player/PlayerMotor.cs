﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerMotor : MonoBehaviour {
    public bool isEnabled { private set; get; }
    public Rigidbody rigidBody;
    public Transform cameraTransform;
    public Transform bodyTransform;
    public float moveSpeed = 5f;

    [Header("Mouse look vars")]
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F, maximumX = 360F, minimumY = -60F,maximumY = 60F;
    [Header("Footstep vars")]
    public bool visualiseFootSteps;
    public AudioSource footstepAudioSource;
    public AudioMixerGroup defaultFootStepMixer;
    public AudioMixerGroup visualFootStepMixer;
    public Vector2 footStepOffset = new Vector2(-1f,1f);
    public GameObject leftFootStepPrefab;
    public float footstepDelay = 0.5f;
    public AudioClip footStepSFX;
    public MinMaxValue footStepPitchChange;
    public int maxFootsteps = 20;

    private bool _canPlayFootstep;   
    private bool _steppingLeft;

    public bool isMoving { get { return _isMoving; } }
    private bool _isMoving = false;
    private float _xMove, _yMove;
    private float rotationX = 0F,rotationY = 0F, _currFootStepDelay = 0f;
    private Quaternion _originalRot;
    private Quaternion _originalBodyRot;
    private List<GameObject> _footSteps = new List<GameObject>();

    void Awake ( ) {
        if ( rigidBody == null || cameraTransform == null || footstepAudioSource == null) {
            Debug.LogError ( "Essential vars to move have not been set, please amend and retry" );
            Destroy ( this );
        }

        _originalRot = cameraTransform.localRotation;
        _originalBodyRot = bodyTransform.rotation;
        if ( footStepSFX != null ) {
            footstepAudioSource.clip = footStepSFX;
        }
        else {
            Debug.Log ( "No footstep sfx has been added to the player, you dummy" );
        }
    }
    public void SetEnabledState ( bool newState ) {
        isEnabled = newState;
    }
    void FixedUpdate ( ) {

        if ( isEnabled ) {
            _xMove = Input.GetAxisRaw ( "Horizontal" );
            _yMove = Input.GetAxisRaw ( "Vertical" );

            Move ( ((transform.right * _xMove) + (transform.forward * _yMove)).normalized * moveSpeed );

            CameraRotation ();
        }
        if ( footstepAudioSource != null && !_canPlayFootstep ) {
            _currFootStepDelay -= Time.deltaTime;
            if ( _currFootStepDelay <= 0 ) {
                _canPlayFootstep = true;
            }
        }
    }

    private void Move ( Vector3 velocity_ ) {
        if ( velocity_ != Vector3.zero ) {
            _isMoving = true;
            rigidBody.MovePosition ( rigidBody.position + velocity_ * Time.fixedDeltaTime );
            if ( _canPlayFootstep ) {
                FootStep ();
            }
        }
        else {
            if ( _isMoving ) {
                FootStep ();
                _isMoving = false;
            }
        }
    }

    private void FootStep ( ) {
        PlayFootStepSFX ();
        if ( visualiseFootSteps ) PlaceFootStep ();
        _steppingLeft = !_steppingLeft;
    }

    private void PlaceFootStep ( ) {
        GameObject g = Instantiate ( leftFootStepPrefab , bodyTransform.localPosition + ((_steppingLeft ? -bodyTransform.right : bodyTransform.right)/4) + Vector3.up/10, bodyTransform.localRotation);
        if ( !_steppingLeft )
            g.transform.localScale = new Vector3 ( -1 , 1 , 1 );
        _footSteps.Add ( g );
        CheckFootstepCount ();
    }

    void CheckFootstepCount ( ) {
        if ( _footSteps.Count > maxFootsteps ) {
            GameObject g = _footSteps[0];
            _footSteps.RemoveAt ( 0 );
            Destroy ( g );
        }
    }

    private void PlayFootStepSFX ( ) {
        footstepAudioSource.outputAudioMixerGroup = visualiseFootSteps ? visualFootStepMixer : defaultFootStepMixer;
        footstepAudioSource.transform.localPosition = new Vector3 ( _steppingLeft ? footStepOffset.x : footStepOffset.y , 0 );
        footstepAudioSource.panStereo = _steppingLeft ? -1f : 1f;
        footstepAudioSource.pitch = Random.Range ( footStepPitchChange.min , footStepPitchChange.max );
        footstepAudioSource.Play ();
        _canPlayFootstep = false;
        _currFootStepDelay = footstepDelay;
    }

    private void CameraRotation ( ) {
        rotationX += Input.GetAxis ( "Mouse X" ) * sensitivityX;
        rotationY += Input.GetAxis ( "Mouse Y" ) * sensitivityY;

        rotationX = ClampAngle ( rotationX , minimumX , maximumX );
        rotationY = ClampAngle ( rotationY , minimumY , maximumY );

        Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, Vector3.left);

        cameraTransform.localRotation = _originalRot * yQuaternion;
        bodyTransform.rotation = _originalBodyRot * xQuaternion;
    }

    public static float ClampAngle ( float angle , float min , float max ) {
        if ( angle < -360F )
            angle += 360F;
        if ( angle > 360F )
            angle -= 360F;
        return Mathf.Clamp ( angle , min , max );
    }
}
