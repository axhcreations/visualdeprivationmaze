﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndTrigger : MonoBehaviour {

    private Collider _trigger;

    private void OnTriggerEnter ( Collider col ) {
        if ( col.gameObject.layer == LayerMask.NameToLayer ( "Player" ) ) {
            Debug.Log ( "Player Entered Trigger" );

            FindObjectOfType<GameManager> ().EndLevel ( true );
        }
    }

}
