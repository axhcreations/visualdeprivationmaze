﻿using UnityEngine;
using System.Collections;

public class ProceduralNumberGenerator {
	public static int currentPosition = 0;
    private static string key = "123424123342421432233144441212334432121223344";
    private const int _keyLength = 30;
    private const int _keyMinVal = 1;
    private const int _keyMaxVal = 4;

    public static void GenerateRandomKey ( ) {
        string newKey = "";
        for ( int i = 0 ; i < _keyLength ; i++ ) {
            newKey += Random.Range ( _keyMinVal , _keyMaxVal + 1 ).ToString ();
        }
        key = newKey;
    }

	public static int GetNextNumber() {
		string currentNum = key.Substring(currentPosition++ % key.Length, 1);
		return int.Parse (currentNum);
	}
}
