﻿[System.Serializable]
public class MinMaxValue {
    public float min;
    public float max;
    public MinMaxValue ( float min_ , float max_ ) {
        min = min_;
        max = max_;
    }
}