﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum WraithBehaviourStates { Waiting, Searching, Chasing, Attacking };

public class WraithController : MonoBehaviour {

    public bool isEnabled { private set; get; }
    public Transform headTransform;
    public WraithBehaviourStates currentBehaviourState = WraithBehaviourStates.Waiting;
    public float aiTickRate = 0.1f;
    public float intersectingWallsCount =0;
    public LayerMask hearingBlockers;
    public float hearingRange =0;
    public float minumumRange = 10;
    [SerializeField]
    private float wallImpactedHearingRange;
    private float hearingangeCheckRate = 0.1f;
    [SerializeField]
    private float wallBlockAmount = 5f;
    [SerializeField]
    private float _distanceToPlayer;
    private NavMeshAgent _navAgent;
    private bool _isMoving;
    private Vector3 _moveTarget;
    private Vector3 _hTorigPos;

    private PlayerController _player;

    private void Awake ( ) {
        _navAgent = GetComponent<NavMeshAgent> ();
        _hTorigPos = headTransform.localPosition;
        _player = FindObjectOfType<PlayerController> ();
    }

    public void SetActiveState ( bool newState ) {
        isEnabled = newState;
        if ( newState ) {
            InvokeRepeating ( "WraithAiTick" , aiTickRate , aiTickRate );
            //SetAiState ( WraithBehaviourStates.Searching );
            StartCoroutine ( "HeadJitter" );
            InvokeRepeating ( "CheckAudibleDistanceToPlayer" , 0 , hearingangeCheckRate );
        }
        else {
            SetAiState ( WraithBehaviourStates.Waiting );
            CancelInvoke ( "WraithAiTick" );
            StopCoroutine ( "HeadJitter" );
            CancelInvoke ( "CheckAudibleDistanceToPlayer" );
        }           
    }

    void CheckAudibleDistanceToPlayer ( ) {
        _distanceToPlayer = Vector3.Distance ( this.transform.position , _player.transform.position );
        Ray ray = new Ray(this.transform.position + Vector3.up, (_player.transform.position-this.transform.position)+ Vector3.up);
        Debug.DrawRay ( ray.origin , ray.direction * _distanceToPlayer );
        RaycastHit[] hits;
        hits = Physics.RaycastAll ( ray , _distanceToPlayer , hearingBlockers );
        wallImpactedHearingRange = hearingRange- (hits.Length *wallBlockAmount);
        if ( wallImpactedHearingRange < minumumRange )
            wallImpactedHearingRange = minumumRange;
        if ( (_distanceToPlayer - _player.audioDistance) < wallImpactedHearingRange ) {
            HeardPlayer ();
        }
    }

    private void OnDrawGizmos ( ) {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere ( transform.position , wallImpactedHearingRange );
    }

    void HeardPlayer ( ) {
        Debug.Log ("Heard youi!");
    }

    private void WraithAiTick ( ) {
        switch ( currentBehaviourState ) {
            case WraithBehaviourStates.Waiting:
                return;

            case WraithBehaviourStates.Chasing:

                break;

            case WraithBehaviourStates.Searching:
                //If no move target is set...
                if ( !_isMoving) {
                    SetMoveTarget ();
                }
                else {
                    //Else if we have a move target and 

                }
                break;

            case WraithBehaviourStates.Attacking:

                break;

        }
    }


    IEnumerator HeadJitter ( ) {
        while ( true ) {
            headTransform.localPosition = _hTorigPos + new Vector3 ( (Mathf.Sin ( Time.time * 4f )/2 ) * Random.Range ( 0.1f , 1f ) , (Mathf.Sin ( Time.time * 5f ) ) * Random.Range ( 0.1f , 0.8f ) , 0 );
            yield return new WaitForSeconds (0.01f );
        }
    }

    private void Update ( ) {
        _isMoving = _navAgent.velocity.magnitude == 0 ? false : true;    
    }



    public void SetAiState ( WraithBehaviourStates newState , float delay = 0f ) {
        if ( delay == 0 ) {
            currentBehaviourState = newState;

        }
        else {
            StartCoroutine ( DelayedSetAiState ( newState , delay ) );
        }
    }

    private IEnumerator DelayedSetAiState ( WraithBehaviourStates newState , float delay ) {
        yield return new WaitForSeconds ( delay );
        SetAiState ( newState , 0 );
    }

    private void SetMoveTarget ( ) {
        _navAgent.destination= PlayerController.playerPosition.position;
        _moveTarget = PlayerController.playerPosition.position;
        Debug.LogError ("WRAITH - MOVING to.." + _moveTarget);
        
    }

}
