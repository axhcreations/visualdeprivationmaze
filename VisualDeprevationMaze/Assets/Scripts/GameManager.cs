﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour {

    public bool debugMode;
    public bool spawnWraith;
    public bool addLighting;

    public GameObject playerPrefab;
    public GameObject goalPrefab;
    public GameObject wraithPrefab;
    public MazeGenerator mazeGenerator;
    public bool gameReady { private set; get; }
    public bool playerReady { private set; get; }
    public bool mazeReady { private set; get; }

    [SerializeField]
    private bool levelEnded = false;

    private PlayerController _player = null;
    private WraithController _wraith = null;
    private WaitForEndOfFrame coroWait = new WaitForEndOfFrame();

    private void Awake ( ) {
        if ( playerPrefab == null || mazeGenerator == null || wraithPrefab == null ) {
            Debug.LogError ( "Required prefabs have not been set, please amend and retry" );
            Destroy ( this );
        }
        
    }

    private void Start ( ) {
        StartCoroutine ( InitGame () );
    }

    private IEnumerator InitGame ( ) {
        Debug.Log ( "Level Init Begin" );
        InitMaze ();
        while ( mazeGenerator.mazeGenerated == false ) { yield return coroWait; }
        //Wait a frame before we try and init the nav mesh
        yield return coroWait;
        InitNavArea ();

        mazeReady = true;

        InitGoal ();
        InitPlayer ();
        while ( _player == null ) { yield return coroWait; }
        playerReady = true;

        if ( spawnWraith ) {
            InitWraith ();
            while ( _wraith == null ) { yield return coroWait; }
        }

        if ( addLighting ) {
            GameObject g = new GameObject();
            g.name = "Directional Light";
            Light l = g.AddComponent<Light> ();
            l.type = LightType.Directional;
            g.transform.eulerAngles = new Vector3 ( 50 , -30 , 0 );
        }

        gameReady = true;

        Debug.Log ( "Level Init Complete" );
        if ( debugMode ) {
            
            BeginGame ();
        }
        else
            StartCoroutine ( BeginGameIntro () );
    }

    private IEnumerator BeginGameIntro ( ) {
        Debug.Log ( "Begin Game Intro" );
        yield return coroWait;
        BeginGame ();
    }


    public void BeginGame ( ) {
        Debug.Log ( "Begin Game" );
        _player.EnablePlayer ( true );
        if ( _wraith != null ) {
            _wraith.SetActiveState ( true );
        }
    }

    private void InitMaze ( ) {
        Debug.Log ( "Init Maze" );
        mazeGenerator.GenerateMaze ( debugMode );
    }

    private void InitNavArea ( ) {
        Debug.Log ( "Init Nav Area" );
        NavMeshSurface nms = mazeGenerator.gameObject.AddComponent<NavMeshSurface> ();
        nms.BuildNavMesh ();
    }

    private void InitGoal ( ) {
        Instantiate ( goalPrefab , mazeGenerator.GetLastCell ().floor.transform.position , Quaternion.identity );
    }

    private void InitPlayer ( ) {
        GameObject g  = Instantiate ( playerPrefab , mazeGenerator.GetFirstCell ().floor.transform.position , Quaternion.identity );
        _player = g.GetComponent<PlayerController> ();
    }

    private void InitWraith ( ) {
        GameObject g = Instantiate(wraithPrefab, mazeGenerator.GetLastCell ().floor.transform.position, Quaternion.identity);
        _wraith = g.GetComponent<WraithController> ();
    }

    public void EndLevel ( bool playerVictory ) {
        Debug.Log ( "Level Ended" );

        if ( _wraith != null ) {
            _wraith.SetActiveState ( false );
        }

        if ( playerVictory ) {
            Debug.Log ( "Player has won" );
            _player.EnablePlayer ( false );
            Invoke ( "ReloadLevel" , 5f );
        }
        else {
            Debug.Log ( "Player has lost" );
        }
    }

    void ReloadLevel ( ) {
        UnityEngine.SceneManagement.SceneManager.LoadScene ( 0 );
    }
}
