﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;

public class AudioEmitterDistanceVolumeChanger : MonoBehaviour {

    public bool isEnabled { private set; get; }
    public MinMaxValue rayDistance = new MinMaxValue(0.1f,1.0f);
    public MinMaxValue aEmMixerVolumeLevels = new MinMaxValue (-80f,0f);
    public AudioSource audioSourceNorth,audioSourceSouth,audioSourceEast,audioSourceWest;
    public float audioSourceMoveSpeed = 5;
    public float northDist,southDist,eastDist,westDist = 0;
    [ SerializeField ]
    private AudioMixer _emitterVolume;
    [SerializeField]
    private Transform _transform;

    private Ray _northRay,_southRay,_eastRay,_westRay;
    private float _moveStep = 0f;
    private Coroutine _currentFadeCoro;

    private void Awake ( ) {
        if ( _transform == null )
            _transform = this.gameObject.transform;
        _eastRay = new Ray ( _transform.position , _transform.right * rayDistance.max );
        _westRay = new Ray ( _transform.position , -_transform.right * rayDistance.max );
        _northRay = new Ray ( _transform.position , _transform.forward * rayDistance.max );
        _southRay = new Ray ( _transform.position , -_transform.forward * rayDistance.max );
        SetEnabledState ( false );
        StartCoroutine ( RepeatPlayNorthAudioSource() );
        StartCoroutine ( RepeatPlaySouthAudioSource () );
        StartCoroutine ( RepeatPlayEastAudioSource () );
        StartCoroutine ( RepeatPlayWestAudioSource () );
    }

    void Update ( ) {
        SetAllAudioSourcePositions ();
    }

    IEnumerator RepeatPlayNorthAudioSource ( ) {
        while ( true ) {
            while ( audioSourceNorth.isPlaying )
                yield return new WaitForEndOfFrame ();
            audioSourceNorth.Play ();
            yield return new WaitForSeconds (northDist);
        }
    }
    IEnumerator RepeatPlaySouthAudioSource ( ) {
        while ( true ) {
            while ( audioSourceSouth.isPlaying )
                yield return new WaitForEndOfFrame ();
            audioSourceSouth.Play ();
            yield return new WaitForSeconds ( southDist );
        }
    }
    IEnumerator RepeatPlayEastAudioSource ( ) {
        while ( true ) {
            while ( audioSourceEast.isPlaying )
                yield return new WaitForEndOfFrame ();
            audioSourceEast.Play ();
            yield return new WaitForSeconds ( eastDist );
        }
    }
    IEnumerator RepeatPlayWestAudioSource ( ) {
        while ( true ) {
            while ( audioSourceWest.isPlaying )
                yield return new WaitForEndOfFrame ();
            audioSourceWest.Play ();
            yield return new WaitForSeconds ( westDist );
        }
    }



    //TODO Wtf is this? Needlessly complex for something minor....
    private int audioFadeSpeed = 10;
    IEnumerator AudioFadeInOut ( bool fadeIn ) {
     //   Debug.Log ( "Begin audio fade - " + fadeIn );
        if ( fadeIn ) {
            float currVal =0 ;
            _emitterVolume.GetFloat ( "emitterVolume" , out currVal );
            while ( currVal != aEmMixerVolumeLevels.max ) {
                currVal += Mathf.Abs ( (aEmMixerVolumeLevels.min + aEmMixerVolumeLevels.max) ) / audioFadeSpeed;
                currVal = Mathf.Clamp ( currVal , aEmMixerVolumeLevels.min , aEmMixerVolumeLevels.max );
                _emitterVolume.SetFloat ( "emitterVolume" , currVal );
                yield return new WaitForSeconds ( 1 / audioFadeSpeed );
            }
        }
        else {
            float currVal =0 ;
            _emitterVolume.GetFloat ( "emitterVolume" , out currVal );
            while ( currVal != aEmMixerVolumeLevels.min ) {
                currVal -= Mathf.Abs ( (aEmMixerVolumeLevels.min + aEmMixerVolumeLevels.max) ) / audioFadeSpeed;
                currVal = Mathf.Clamp ( currVal , aEmMixerVolumeLevels.min , aEmMixerVolumeLevels.max );
                _emitterVolume.SetFloat ( "emitterVolume" , currVal );
                yield return new WaitForSeconds ( 1 / audioFadeSpeed );
            }
        }
    }

    public void SetEnabledState ( bool newState ) {
        Debug.Log ( "Setting audio emitter states to " + newState );
        isEnabled = newState;
        if ( _currentFadeCoro != null )
            StopCoroutine ( _currentFadeCoro );
        _currentFadeCoro = StartCoroutine ( AudioFadeInOut ( newState ) );
        //_emitterVolume.SetFloat ( "emitterVolume" , newState ? aEmMixerVolumeLevels.max : aEmMixerVolumeLevels.min );
    }

    private void SetAllAudioSourcePositions ( ) {
        _moveStep = audioSourceMoveSpeed * Time.deltaTime;
        audioSourceEast.transform.position = Vector3.MoveTowards ( audioSourceEast.transform.position , SetAudioSourcePos ( _eastRay , _transform.right ) , _moveStep );
        audioSourceWest.transform.position = Vector3.MoveTowards ( audioSourceWest.transform.position , SetAudioSourcePos ( _westRay , -_transform.right ) , _moveStep );
        audioSourceNorth.transform.position = Vector3.MoveTowards ( audioSourceNorth.transform.position , SetAudioSourcePos ( _northRay , _transform.forward ) , _moveStep );
        audioSourceSouth.transform.position = Vector3.MoveTowards ( audioSourceSouth.transform.position , SetAudioSourcePos ( _southRay , -_transform.forward ) , _moveStep );
        northDist = Vector3.Distance ( audioSourceNorth.transform.position , transform.position )/5;
        southDist = Vector3.Distance ( audioSourceSouth.transform.position , transform.position )/5;
        eastDist = Vector3.Distance ( audioSourceEast.transform.position , transform.position )/5;
        westDist = Vector3.Distance ( audioSourceWest.transform.position , transform.position )/5;
    }

    private Vector3 SetAudioSourcePos ( Ray ray_ , Vector3 direction_ ) {
        ray_.origin = _transform.position;
        ray_.direction = direction_ * rayDistance.max;
        RaycastHit hit;
        if ( Physics.Raycast ( ray_ , out hit , rayDistance.max ) ) {
            Debug.DrawLine ( ray_.origin , hit.point , Color.green );
            //Debug.Log ( ray_ + " " + hit.collider.gameObject.name );
            return hit.point;
        }
        else {
            Debug.DrawLine ( ray_.origin , ray_.origin + (direction_ * rayDistance.max) , Color.blue );
            return ray_.origin + (direction_ * rayDistance.max);
        }
    }
}
