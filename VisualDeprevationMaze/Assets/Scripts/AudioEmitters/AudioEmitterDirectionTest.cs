﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEmitterDirectionTest : MonoBehaviour {

    public AudioSource asNorth;
    public AudioSource asSouth;
    public AudioSource asEast;
    public AudioSource asWest;

    void Awake ( ) {
        if ( asNorth == null || asSouth == null || asEast == null || asWest == null ) {
            Debug.LogError ("AudioSources have not been set correctly, please amend and retry");
            Destroy ( this );
        }
    }

    void Update () {
        OnUpdate ();	
	}

    public virtual void OnUpdate ( ) {
        AudioEmitterTestInput ();
    }

    public void AudioEmitterTestInput ( ) {
        //North
        if ( Input.GetKeyDown ( KeyCode.Keypad8 ) ) {
            if ( !asNorth.isPlaying )
                asNorth.Play ();
        }
        if ( Input.GetKeyUp ( KeyCode.Keypad8 ) ) {
            if ( asNorth.isPlaying )
                asNorth.Stop ();
        }
        //South
        if ( Input.GetKeyDown ( KeyCode.Keypad2 ) ) {
            if ( !asSouth.isPlaying )
                asSouth.Play ();
        }
        if ( Input.GetKeyUp ( KeyCode.Keypad2 ) ) {
            if ( asSouth.isPlaying )
                asSouth.Stop ();
        }
        //East
        if ( Input.GetKeyDown ( KeyCode.Keypad6 ) ) {
            if ( !asEast.isPlaying )
                asEast.Play ();
        }
        if ( Input.GetKeyUp ( KeyCode.Keypad6 ) ) {
            if ( asEast.isPlaying )
                asEast.Stop ();
        }
        //West
        if ( Input.GetKeyDown ( KeyCode.Keypad4 ) ) {
            if ( !asWest.isPlaying )
                asWest.Play ();
        }
        if ( Input.GetKeyUp ( KeyCode.Keypad4 ) ) {
            if ( asWest.isPlaying )
                asWest.Stop ();
        }
    }
}
